---
date: 2016-08-05 17:05:01 -0700
title: About
description: About the author and site
sidemenu: true
type: page

---
Mark is a human being. He lives in Edmonds, a small town in Washington State – United States of America. His home provides the advantages of a great city and a small coastal town in a region nestled between mountain ranges, divided by the Salish Sea, and a series of freshwater lakes and rivers. Mark lived in Italy for a year in the early 1980s and in Turkey for a year in the early 1990s. He earned a degree in political science at The University at Albany, State University of New York. Today he works on cloud-native computing technologies in Seattle USA.

_The views expressed here are his own. They are not endorsed, approved, or reviewed by any other person or organization._

# Contact Information

```plain
MARK AYERS
712 ALDER ST
EDMONDS WA 98020-3416
UNITED STATES OF AMERICA

mark@philoserf.com
+ 1.206.280.4061
```

**github:** [**@philoserf**](http://github.com/philoserf) **| email:** [**mark@philoserf.com**](mailto:mark@philoserf.com)