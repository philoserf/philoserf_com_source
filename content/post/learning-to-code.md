---
description: Learning to program is like learning to read and write any foreign language.
  Except, unlike spoken language, there are no translations to some language you already
  know.
tags:
- programming
- code
- language
- learning
topics:
- Reflection
title: Learning to Code
date: 2017-12-22 00:00:01 -0800

---
Learning to program is like learning to read and write any foreign language. Lots of time will be spent looking at symbols that make little sense and hearing people speak about the language in terms that are, while familiar, carrying less meaning than perhaps they should. Lots of time will be spent reading references without context, then looking at language in context, then back and forth again.

Except, unlike spoken language, there are no translations to some language you already know.

But one day, without reason, the symbols will begin to fit together. Words are recognized. The meaning of statements become clearer. Whole paragraphs express simple or complex ideas.

At first, in this new stage, some parts are understandable and others remain opaque. But you’ve learned to use the references better. You are beginning to recognize that not all the uses of the language you see are at the same level of creative expression.

Later when you are proficient but not yet fluent you will recognize the convoluted and the needlessly complex expressions. You will also begin to recognize the poetry.

You cannot write a movie review in English as well as Roger Ebert did without lots of time using the language in the way he did. You cannot expect to write meaningful code until you came see language as expression of ideas you really understand.

---

_in response to_ [Here’s Why Learning How To Code Is So Hard (and what to do about it) by Kevin Kononenko](https://medium.com/the-mission/heres-why-learning-how-to-code-is-so-hard-and-what-to-do-about-it-3d6fda152409 "Here’s Why Learning How To Code Is So Hard (and what to do about it) by Kevin Kononenko")
