---
date: 2017-01-23 00:00:01 -0700
title: Bash Shell Script Style Guide
description: Code style matters
tags:
- bash
- shell
- script
- style
- quality
topics:
- Technology

---
We drive our devops system of CI/CD pipelines with a collection of standardized bash shell scripts. These scripts collect required resources; validate starting conditions; check for lint and style issues; run unit, acceptance, integration, performance, and smoke tests; build deployment packages and supporting infrastructure in various environments; all while collecting metrics on the artifacts being built and the pipeline's execution; and reporting the success and failure of each pipeline stage, job, and task into our dedicated chatops channels.

Here is our base shell style guide.

<https://google.github.io/styleguide/shell.xml>
