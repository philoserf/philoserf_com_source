---
date: 2017-02-05 00:00:01 -0800
title: Good Code
description: Code quality matters
tags:
- Quality
- Code
topics:
- Technology

---
Good Code

1. Works
2. Is easy to understand
3. Is easy to change
4. Is fun to work with

Reference: https://robots.thoughtbot.com/what-is-good-code

In fairness, this view is valid too: https://xkcd.com/844/

With the existing code bases we work on and the code bases we create, I am going to work tirelessly to improve the first case and contribute the later.
