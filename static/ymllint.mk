# install yamllint
install-yamllint:
	pip install yamllint
.PHONY: install-yamllint

yml_files := $(shell git ls-files|grep -e \\.yml$)

# check yaml files for lint, format, and errors
yamllint:
	yamllint $(yml_files)
.PHONY: yamllint
